package number26;

import de.number26.transaction.Application;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class TransactionsAPIIntegrationTest {
    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    @Autowired
    private WebApplicationContext webApplicationContext;
    private MockMvc mockMvc;

    private static long currentId = 0L;
    private long parentId = nextId();
    private long childId = nextId();
    private MockHttpServletRequestBuilder createTransactionRequest =
            put("/transactionservice/transaction/" + parentId)
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content("{ \"amount\": 10.0,\"type\": \"grocery" + parentId + "\"}");
    private MockHttpServletRequestBuilder createChildTransaction =
            put("/transactionservice/transaction/" + childId)
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content("{ \"amount\": 5.0,\"type\": \"drinks" + childId + "\", \"parent_id\": " + parentId + "}");


    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void storeTransaction() throws Exception {
        createTransactionAndAssertThatTheAnswerIsOk();
    }

    @Test
    public void storeTransactionWithParentId() throws Exception {
        saveParentAndChildTransactions();
    }

    @Test
    public void getParentTransaction() throws Exception {

        createTransactionAndAssertThatTheAnswerIsOk();

        mockMvc.perform(createGetTransactionRequest(parentId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("amount").value(10.0))
                .andExpect(jsonPath("type").value("grocery" + parentId))
                .andExpect(jsonPath("parent_id").doesNotExist());
    }

    @Test
    public void getChildTransaction() throws Exception {
        saveParentAndChildTransactions();

        mockMvc.perform(createGetTransactionRequest(childId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("amount").value(5.0))
                .andExpect(jsonPath("type").value("drinks" + childId))
                .andExpect(jsonPath("parent_id").value(((int) parentId)));
    }

    @Test
    public void getParentTransactionsByType() throws Exception {
        saveParentAndChildTransactions();

        mockMvc.perform(get("/transactionservice/types/grocery" + parentId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$").value(((int) parentId)));
    }

    @Test
    public void getChildTransactionByType() throws Exception {
        saveParentAndChildTransactions();

        mockMvc.perform(get("/transactionservice/types/drinks" + childId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$").value(((int) childId)));
    }

    @Test
    public void getTransactionsSum() throws Exception {
        createTransactionAndAssertThatTheAnswerIsOk();

        mockMvc.perform(getSumRequest(parentId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("sum").value(10.0));
    }

    @Test
    public void getChildTransactionSum() throws Exception {
        saveParentAndChildTransactions();

        mockMvc.perform(getSumRequest(childId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("sum").value(5.0));
    }

    @Test
    public void getParentAndChildTransactionsSum() throws Exception {
        saveParentAndChildTransactions();

        mockMvc.perform(getSumRequest(parentId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("sum").value(15.0));
    }

    private MockHttpServletRequestBuilder getSumRequest(long transactionId) {
        return get("/transactionservice/sum/" + transactionId);
    }

    private MockHttpServletRequestBuilder createGetTransactionRequest(long transactionId) {
        return get("/transactionservice/transaction/" + transactionId);
    }

    private void saveParentAndChildTransactions() throws Exception {
        createTransactionAndAssertThatTheAnswerIsOk();
        createTransactionAndAssertThatTheAnswerIsOk(createChildTransaction);
    }

    private void createTransactionAndAssertThatTheAnswerIsOk() throws Exception {
        mockMvc.perform(createTransactionRequest)
                .andExpect(status().isOk())
                .andExpect(jsonPath("status").value("ok"));
    }

    private void createTransactionAndAssertThatTheAnswerIsOk(MockHttpServletRequestBuilder createTransactionRequest)
            throws Exception {
        mockMvc.perform(createTransactionRequest)
                .andExpect(status().isOk())
                .andExpect(jsonPath("status").value("ok"));
    }

    /*
        There should be a transaction rollback/data wipe out.
        Let's keep the data to not produce additional code
          for this little exercise
     */
    private static long nextId() {
        return currentId++;
    }
}
