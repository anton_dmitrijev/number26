package de.number26.transaction.controller;

import de.number26.transaction.domain.Transaction;
import de.number26.transaction.request.CreateTransactionRequest;
import de.number26.transaction.response.TransactionCreationResponse;
import de.number26.transaction.response.TransactionDataResponse;
import de.number26.transaction.response.TransactionSumResponse;
import de.number26.transaction.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@RestController
@RequestMapping(value = "/transactionservice", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @RequestMapping(
            value = "/transaction/{transactionId}",
            method = PUT
    )
    TransactionCreationResponse createTransaction(@PathVariable long transactionId,
                                                  @RequestBody CreateTransactionRequest ctr) {
        Transaction transactionToSave = new Transaction(
                transactionId, ctr.getAmount(),
                ctr.getType(), ctr.getParentId()
        );
        transactionService.save(transactionToSave);
        return new TransactionCreationResponse("ok");
    }

    @RequestMapping(
            value = "/transaction/{transactionId}",
            method = GET
    )
    TransactionDataResponse getTransaction(@PathVariable long transactionId) {
        Transaction transaction = transactionService.get(transactionId);
        return new TransactionDataResponse(transaction);
    }

    @RequestMapping(
            value = "/types/{type}",
            method = GET
    )
    Long[] getTransactionByType(@PathVariable String type) {
        return transactionService.findByType(type);
    }

    @RequestMapping(
            value = "/sum/{transactionId}",
            method = GET
    )
    TransactionSumResponse getTransactionSum(@PathVariable long transactionId) {
        return new TransactionSumResponse(transactionService.sum(transactionId));
    }
}
