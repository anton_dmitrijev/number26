package de.number26.transaction.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.number26.transaction.domain.Transaction;

import java.math.BigDecimal;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TransactionDataResponse {
    @JsonProperty("parent_id")
    private final Long parentId;
    private final String type;
    private final BigDecimal amount;

    public TransactionDataResponse(Transaction transaction) {
        this.amount = transaction.getAmount();
        this.type = transaction.getType();
        this.parentId = transaction.getParentId();
    }

    public Long getParentId() {
        return parentId;
    }

    public String getType() {
        return type;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
