package de.number26.transaction.response;

import java.math.BigDecimal;

public class TransactionSumResponse {
    private final BigDecimal sum;

    public TransactionSumResponse(BigDecimal sum) {
        this.sum = sum;
    }

    public BigDecimal getSum() {
        return sum;
    }
}
