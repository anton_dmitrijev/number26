package de.number26.transaction.response;

public class TransactionCreationResponse {
    private final String status;

    public TransactionCreationResponse(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
