package de.number26.transaction.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class CreateTransactionRequest {
    private BigDecimal amount;
    private String type;
    @JsonProperty("parent_id")
    private Long parentId;

    public BigDecimal getAmount() {
        return amount;
    }

    public String getType() {
        return type;
    }

    public Long getParentId() {
        return parentId;
    }
}
