package de.number26.transaction.service;

import de.number26.transaction.domain.Transaction;

import java.math.BigDecimal;

public interface TransactionService {

    boolean save(Transaction transaction);

    Transaction get(long id);

    Long[] findByType(String type);

    BigDecimal sum(long id);
}
