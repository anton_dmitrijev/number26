package de.number26.transaction.domain;

import java.math.BigDecimal;

public class Transaction {
    private final long id;
    private final BigDecimal amount;
    private final String type;
    private final Long parentId;

    public Transaction(long id, BigDecimal amount, String type, Long parentId) {
        this.id = id;
        this.amount = amount;
        this.type = type;
        this.parentId = parentId;
    }

    public Transaction(long id, BigDecimal amount, String type) {
        this(id, amount, type, null);
    }

    public long getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getType() {
        return type;
    }

    public Long getParentId() {
        return parentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Transaction that = (Transaction) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
