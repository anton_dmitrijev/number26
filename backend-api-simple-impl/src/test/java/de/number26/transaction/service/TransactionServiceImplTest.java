package de.number26.transaction.service;

import de.number26.transaction.domain.Transaction;
import org.junit.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class TransactionServiceImplTest {
    private TransactionServiceImpl transactionRepository = new TransactionServiceImpl();
    private Transaction parentTransaction = new Transaction(1L, BigDecimal.ONE, "food");
    private Transaction childTransaction = new Transaction(2L, BigDecimal.TEN, "food", parentTransaction.getId());

    @Test
    public void saveTransaction() {
        boolean saveResult = saveParentTransaction();

        assertThat(saveResult).isTrue();
    }

    @Test
    public void saveParentTransactionAndRetrieveItBack() {
        saveParentTransaction();

        Transaction retrievedTransaction = transactionRepository.get(parentTransaction.getId());

        assertThat(retrievedTransaction).isEqualTo(parentTransaction);
    }

    @Test
    public void findTransactionByType() {
        saveParentTransaction();

        Long[] transactions = transactionRepository.findByType("food");

        assertThat(transactions).containsOnly(parentTransaction.getId());
    }

    @Test
    public void whenSearchingForTransactionByType_andThereAreNoMatchedTransactions_returnEmptyArray() {
        saveParentTransaction();

        Long[] transactions = transactionRepository.findByType("drinks");

        assertThat(transactions).isEmpty();
    }

    @Test
    public void sumParentTransactions() {
        saveParentAndChildTransactions();

        BigDecimal sum = transactionRepository.sum(parentTransaction.getId());

        assertThat(sum).isEqualTo(parentTransaction.getAmount().add(childTransaction.getAmount()));
    }

    @Test
    public void sumChildTransactions() {
        saveParentAndChildTransactions();

        BigDecimal sum = transactionRepository.sum(childTransaction.getId());

        assertThat(sum).isEqualTo(childTransaction.getAmount());
    }

    private void saveParentAndChildTransactions() {
        saveParentTransaction();
        saveChildTransaction();
    }

    private void saveChildTransaction() {
        transactionRepository.save(childTransaction);
    }

    private boolean saveParentTransaction() {
        return transactionRepository.save(parentTransaction);
    }
}