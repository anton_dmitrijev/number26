package de.number26.transaction.service;

import de.number26.transaction.domain.Transaction;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class TransactionServiceImpl implements TransactionService {
    private Set<Transaction> transactions = ConcurrentHashMap.newKeySet();

    /*
    Adding element is a cheap operation unless introduce below mentioned indices.
    Update of every index will decrease performance.
     */
    public boolean save(Transaction transaction) {
        return transactions.add(transaction);
    }

    /*
        There should be the index by primary key. e.g. Map<Long, Transaction>
     */
    public Transaction get(long id) {
        return transactions.stream()
                .filter(t -> t.getId() == id)
                .findFirst()
                .get();
    }

    /*
    Here we go through the whole collection. A better solution should create an index on type.
    For instance Map<TransactionType, Transaction>
     */
    public Long[] findByType(String type) {
        return transactions.stream()
                .filter(t -> t.getType().equals(type))
                .map(Transaction::getId)
                .toArray(Long[]::new);
    }

    /*
    Again we go through all transactions.
    A better solution would be to index transactions based on parent id.
    E.g. Map<Long, Transaction>
     */
    public BigDecimal sum(long id) {
        return transactions.stream()
                .filter(t -> t.getId() == id || (t.getParentId() != null && t.getParentId() == id))
                .map(Transaction::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
